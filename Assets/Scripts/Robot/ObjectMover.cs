using System;
using System.Collections;
using UnityEngine;

namespace Robot
{
    public class ObjectMover: MonoBehaviour
    {
        public interface MoveFinishedListener
        {
            void Execute();
        }

        public bool IsMoving { get; private set; }

        private Action _moveFinishedListener;

        public void MoveTo(Transform objectToMove, Vector3 targetPosition, float moveSpeed)
        {
            IsMoving = true;
            StopCoroutine(MoveObject(objectToMove, targetPosition, moveSpeed));
            StartCoroutine(MoveObject(objectToMove, targetPosition, moveSpeed));
        }

        public void MoveTo(Transform objectToMove, Vector3 targetPosition, float moveSpeed, Action moveFinishedListener)
        {
            _moveFinishedListener = moveFinishedListener;
            MoveTo(objectToMove, targetPosition, moveSpeed);
        }

        private IEnumerator MoveObject(Transform objectToMove, Vector3 targetPosition,
            float moveSpeed)
        {
            var currentProgress = 0f;
            var cashedObjectPosition = objectToMove.transform.position;

            while (currentProgress <= 1f)
            {
                currentProgress += moveSpeed * Time.deltaTime;
                objectToMove.position = Vector3.Lerp(
                    cashedObjectPosition,
                    targetPosition,
                    currentProgress
                );
                yield return null;
            }

            IsMoving = false;
            if (_moveFinishedListener != null)
            {
                _moveFinishedListener();
            }
        }

    }
}
