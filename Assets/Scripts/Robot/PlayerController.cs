﻿using System;
using UnityEngine;

namespace Robot
{
    public class PlayerController : MonoBehaviour
    {
        public float runSpeed = 5f;
        public float maxSpeed = 20f;

        private Animator _animator;
        private bool _facingRight = true;
        private bool _hasJumped;
        private Rigidbody2D _rb;

        private static readonly int Speed = Animator.StringToHash("Speed");

        private void Start()
        {
            _animator = GetComponent<Animator>();
            _rb = GetComponent<Rigidbody2D>();
        }

        private void UpdateDirection(float horizontalMove)
        {
            if (horizontalMove > 0 && !_facingRight)
            {
                Flip();
            }
            else if (horizontalMove < 0 && _facingRight)
            {
                Flip();
            }
        }

        private void Flip()
        {
            _facingRight = !_facingRight;
            var scale = transform.localScale;
            scale.x *= -1;
            transform.localScale = scale;
        }
    }
}
