using UnityEngine;

namespace Robot
{
    public enum Direction
    {
        Left,
        Right
    }

    [RequireComponent(typeof(ObjectMover))]
    public class MoveCommand : MonoBehaviour
    {
        public int steps;

        public Direction direction;

        public float stepLength = 0.5f;

        private static readonly int Speed = Animator.StringToHash("Speed");

        private void OnEnable()
        {
            var mover = gameObject.GetComponent<ObjectMover>();
            var animator = gameObject.GetComponent<Animator>();

            var position = gameObject.transform.position;
            var targetPosition =
                position.x +
                steps * stepLength * (direction == Direction.Left ? -1f : 1f);

            animator.SetFloat(Speed, 1f);
            mover.MoveTo(
                gameObject.transform,
                new Vector3(
                    targetPosition,
                    position.y,
                    position.z
                ),
                1f,
                () => { animator.SetFloat(Speed, 0f); }
            );
        }
    }
}
